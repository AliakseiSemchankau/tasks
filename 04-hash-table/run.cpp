#include <thread>
#include <vector>

#include <benchmark/benchmark.h>
#include <benchmark/benchmark_api.h>
#include <concurrent_hash_map.h>
#include "params.h"
#include "commons.h"

void random_insertions(benchmark::State& state) {
    int query_count = BENCH_MAX_ITERATIONS;
    DummyLogger dummy;
    while (state.KeepRunning()) {
        int threads_count = state.range(0);
        ConcurrentHashMap<int, int> table(query_count * threads_count, threads_count);
        std::vector<std::thread> threads;
        threads.reserve(threads_count);

        for (int i = 0; i < threads_count; ++i)
            threads.emplace_back(make_queries<Random, DummyLogger>, std::ref(table),
                                 std::ref(dummy),
                                 query_count, QueryType::INSERT, Random(SEED + i * 10));

        for (int i = 0; i < threads_count; ++i)
            threads[i].join();
    }
}

void special_insertions(benchmark::State& state) {
    int query_count = BENCH_MAX_ITERATIONS;
    DummyLogger dummy;
    while (state.KeepRunning()) {
        int threads_count = state.range(0);
        ConcurrentHashMap<int, int> table(ConcurrentHashMap<int, int>::kUndefinedSize,
                                          threads_count);
        std::vector<std::thread> threads;
        threads.reserve(threads_count);
        for (int i = 0; i < threads_count - 1; ++i)
            threads.emplace_back(make_queries<Increment, DummyLogger>, std::ref(table),
                                 std::ref(dummy),
                                 query_count, QueryType::INSERT,
                                 Increment(i * BENCH_MAX_ITERATIONS / 10));
        threads.emplace_back(make_queries<EqualLowBits, DummyLogger>, std::ref(table), std::ref(dummy),
                                 query_count, QueryType::INSERT, EqualLowBits(16));

        for (int i = 0; i < threads_count; ++i)
            threads[i].join();
    }
}

void many_searches(benchmark::State& state) {
    int query_count = BENCH_MAX_ITERATIONS;
    DummyLogger dummy;
    while (state.KeepRunning()) {
        int threads_count = state.range(0);
        ConcurrentHashMap<int, int> table(query_count * 2, threads_count);
        make_queries(table, dummy, query_count, QueryType::INSERT, Increment(0));
        std::vector<std::thread> threads;
        threads.reserve(threads_count - 1);
        for (int i = 0; i < threads_count - 1; ++i)
            threads.emplace_back(make_queries<Random, DummyLogger>, std::ref(table), std::ref(dummy),
                                 query_count, QueryType::FIND,
                                 Random(SEED + i, query_count / 2, query_count * 3 / 2));
        threads.emplace_back(make_queries<Random, DummyLogger>, std::ref(table), std::ref(dummy),
                             query_count, QueryType::INSERT, Random(SEED - 1, 0, query_count));

        for (int i = 0; i < threads_count; ++i)
            threads[i].join();
    }
}

void deletions(benchmark::State& state) {
    int query_count = BENCH_MAX_ITERATIONS;
    DummyLogger dummy;
    while (state.KeepRunning()) {
        int threads_count = state.range(0);
        ConcurrentHashMap<int, int> table(ConcurrentHashMap<int, int>::kUndefinedSize,
                                          threads_count);
        std::vector<std::thread> threads;
        threads.reserve(threads_count);
        for (int i = 0; i < threads_count / 2; ++i) {
            threads.emplace_back(make_queries<Random, DummyLogger>, std::ref(table),
                                 std::ref(dummy),
                                 query_count, QueryType::INSERT, Random(SEED - i - 1));
            threads.emplace_back(make_queries<Random, DummyLogger>, std::ref(table),
                                 std::ref(dummy),
                                 query_count, QueryType::ERASE, Random(SEED - i - 1));
        }

        for (int i = 0; i < threads_count; ++i)
            threads[i].join();
    }
}

BENCHMARK(random_insertions)
    ->Arg(2)->Arg(4)->Arg(8)
    ->Unit(benchmark::kMillisecond)
    ->UseRealTime()
    ->MinTime(-1.0);

BENCHMARK(special_insertions)
    ->Arg(2)->Arg(4)->Arg(8)
    ->Unit(benchmark::kMillisecond)
    ->UseRealTime()
    ->MinTime(-1.0);

BENCHMARK(many_searches)
    ->Arg(2)->Arg(4)->Arg(8)
    ->Unit(benchmark::kMillisecond)
    ->UseRealTime()
    ->MinTime(-1.0);

BENCHMARK(deletions)
    ->Arg(2)->Arg(4)->Arg(8)
    ->Unit(benchmark::kMillisecond)
    ->UseRealTime()
    ->MinTime(-1.0);

BENCHMARK_MAIN();
