#include "params.h"

const int BENCH_MAX_ITERATIONS = 300000;
const int TEST_MAX_ITERATIONS = 10000;
const int SEED = 723834;
