cmake_minimum_required(VERSION 2.8)
project(semaphore)

if (TEST_SOLUTION)
  include_directories(../private/semaphore/${TEST_SOLUTION})
endif()

include(../common.cmake)

add_gtest(test_semaphore test.cpp)

#add_benchmark(bench_isprime run.cpp)
#target_link_libraries(bench_isprime is_prime)
