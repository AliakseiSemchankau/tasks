#!/usr/bin/python3

import argparse
import gitlab
import requests
import sys
import os
import logging
import pprint


GITLAB_GROUP="shad-cpp-course"
STUDENT_RUNNER="shad-runner-for-students"
GITLAB_TOKEN_ENV="SHAD_GITLAB_TOKEN"


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(description='Manage student repositories on gitlab.com')
    subparsers = parser.add_subparsers(dest='cmd')
    subparsers.required = True

    create = subparsers.add_parser("create", help="Create and configure repository for student")
    create.add_argument("--username", type=str, help="gitlab.com username of student", required=True)

    return parser.parse_args()


def get_gitlab():
    if GITLAB_TOKEN_ENV not in os.environ:
        exit(GITLAB_TOKEN_ENV + " is not set")

    gl = gitlab.Gitlab("https://gitlab.com", os.environ[GITLAB_TOKEN_ENV])
    return gl



def create_project(gl, args):
    users = gl.users.list(username=args.username)
    if len(users) == 0:
        raise ValueError("No user with username " + args.username)

    student = users[0]

    course_group = None
    for group in gl.groups.search(GITLAB_GROUP):
        if group.name == GITLAB_GROUP:
            course_group = group

    student_project_name = "student-" + student.username
    student_project = None
    logger.info("Looking for project {}".format(student_project_name))
    for project in course_group.projects.list(search=student_project_name):
        if project.name == student_project_name:
            logger.info("Found existing project. id={}".format(project.id))
            student_project = gl.projects.get(project.id)

    if student_project is None:
        logger.info("Existing project not found, creating new.")
        student_project = gl.projects.create({
            "name": student_project_name,
            "namespace_id": course_group.id,
            "builds_enabled": True,
        })

    if student_project.shared_runners_enabled:
        logger.info("Disabling shared runners")
        student_project.shared_runners_enabled = False
        student_project.save()
    else:
        logger.info("Shared runners already disabled")

    logger.info("Looking for runner {}".format(STUDENT_RUNNER))
    runner = None
    for r in gl.runners.list():
        if r.description == STUDENT_RUNNER:
            runner = r
            logger.info("Found runner. id={}".format(runner.id))

    if runner is None:
        raise ValueError("Runner not found")

    logger.info("Enabling runner in student project")
    rsp = gl._raw_post("/projects/{}/runners".format(student_project.id), data={"runner_id": runner.id})
    if rsp.status_code == 201:
        logger.info("Runner activated")
    elif rsp.status_code == 409:
        logger.info("Runner already activated for this project")
    else:
        rsp.raise_for_status()

    for member in student_project.members.list():
        if member.id == student.id:
            logger.info("User already project member")
            break
    else:
        logger.info("Adding user to project")
        student_project.members.create({
            "user_id": student.id,
            "access_level": gitlab.DEVELOPER_ACCESS,
        })
    

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s',level=logging.INFO)

    gl = get_gitlab()
        
    args = parse_args()
    if args.cmd == "create":
        create_project(gl, args)
        
