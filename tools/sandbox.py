#!/usr/bin/python3
import subprocess
import sys
import os
import pwd
import grp
import resource

ENV_WHITELIST = ["PATH"]
MEMORY_LIMIT = 1024 ** 3 # 1 GB
TIMEOUT_SECONDS = 5

def drop_priv():
    uid = pwd.getpwnam("nobody").pw_uid
    gid = grp.getgrnam("nogroup").gr_gid

    resource.setrlimit(resource.RLIMIT_AS, (MEMORY_LIMIT, MEMORY_LIMIT))
    resource.setrlimit(resource.RLIMIT_CPU, (TIMEOUT_SECONDS, TIMEOUT_SECONDS))
    resource.setrlimit(resource.RLIMIT_NPROC, (1000, 1000))
    
    os.setgroups([])
    os.setresgid(gid, gid, gid)
    os.setresuid(uid, uid, uid)

    env = os.environ.copy()
    os.environ.clear()
    for variable in ENV_WHITELIST:
        os.environ[variable] = env[variable]
    
def main():
    subprocess.run(sys.argv[1:],
                   preexec_fn=drop_priv,
                   timeout=TIMEOUT_SECONDS)

if __name__ == "__main__":
    main()
